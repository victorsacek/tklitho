# TKLitho #

Thermo Kinematic Lithosphere

### Steps to use the code ###

* [Install PETSc](https://www.mcs.anl.gov/petsc/)
* In the code directory, modify the first two lines of the makefile to the appropriate directory for PETSc.
* Compile the code in the code/ directory:
```bash
make all 
```
